package com.bbva.arq.front.spring.wxyz.empleweb.web;

import com.bbva.arq.front.spring.wxyz.empleweb.dominio.Empleado;
import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RooWebScaffold(path = "empleadoes", formBackingObject = Empleado.class)
@RequestMapping("/empleadoes")
@Controller
public class EmpleadoController {
}
