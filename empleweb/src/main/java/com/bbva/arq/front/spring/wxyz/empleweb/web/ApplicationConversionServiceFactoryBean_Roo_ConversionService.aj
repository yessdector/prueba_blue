// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.arq.front.spring.wxyz.empleweb.web;

import com.bbva.arq.front.spring.wxyz.empleweb.dominio.Empleado;
import com.bbva.arq.front.spring.wxyz.empleweb.dominio.Tarea;
import java.lang.String;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

privileged aspect ApplicationConversionServiceFactoryBean_Roo_ConversionService {
    
    public void ApplicationConversionServiceFactoryBean.installLabelConverters(FormatterRegistry registry) {
        registry.addConverter(new EmpleadoConverter());
        registry.addConverter(new TareaConverter());
    }
    
    public void ApplicationConversionServiceFactoryBean.afterPropertiesSet() {
        super.afterPropertiesSet();
        installLabelConverters(getObject());
    }
    
    static class com.bbva.arq.front.spring.wxyz.empleweb.web.ApplicationConversionServiceFactoryBean.EmpleadoConverter implements Converter<Empleado, String> {
        public String convert(Empleado empleado) {
            return new StringBuilder().append(empleado.getNombre()).append(" ").append(empleado.getPrimerApellido()).append(" ").append(empleado.getSegundoApellido()).toString();
        }
        
    }
    
    static class com.bbva.arq.front.spring.wxyz.empleweb.web.ApplicationConversionServiceFactoryBean.TareaConverter implements Converter<Tarea, String> {
        public String convert(Tarea tarea) {
            return new StringBuilder().append(tarea.getNombre()).append(" ").append(tarea.getDescripcion()).toString();
        }
        
    }
    
}
