package com.bbva.arq.front.spring.wxyz.empleweb.dominio;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.Size;
import com.bbva.arq.front.spring.wxyz.empleweb.dominio.Empleado;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooEntity
public class Tarea {

    @Size(max = 25)
    private String nombre;

    @Size(max = 150)
    private String descripcion;

    @ManyToOne
    private Empleado propietario;
}
