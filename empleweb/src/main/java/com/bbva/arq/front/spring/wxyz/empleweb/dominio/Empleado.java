package com.bbva.arq.front.spring.wxyz.empleweb.dominio;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.Size;

@RooJavaBean
@RooToString
@RooEntity
public class Empleado {

    @Size(max = 10)
    private String nombre;

    @Size(max = 15)
    private String primerApellido;

    @Size(max = 15)
    private String segundoApellido;
}
