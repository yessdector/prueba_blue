// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.arq.front.spring.wxyz.empleweb.web;

import com.bbva.arq.front.spring.wxyz.empleweb.dominio.Empleado;
import java.io.UnsupportedEncodingException;
import java.lang.Integer;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect EmpleadoController_Roo_Controller {
    
    @RequestMapping(method = RequestMethod.POST)
    public String EmpleadoController.create(@Valid Empleado empleado, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            uiModel.addAttribute("empleado", empleado);
            return "empleadoes/create";
        }
        uiModel.asMap().clear();
        empleado.persist();
        return "redirect:/empleadoes/" + encodeUrlPathSegment(empleado.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", method = RequestMethod.GET)
    public String EmpleadoController.createForm(Model uiModel) {
        uiModel.addAttribute("empleado", new Empleado());
        return "empleadoes/create";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String EmpleadoController.show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("empleado", Empleado.findEmpleado(id));
        uiModel.addAttribute("itemId", id);
        return "empleadoes/show";
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String EmpleadoController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            uiModel.addAttribute("empleadoes", Empleado.findEmpleadoEntries(page == null ? 0 : (page.intValue() - 1) * sizeNo, sizeNo));
            float nrOfPages = (float) Empleado.countEmpleadoes() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("empleadoes", Empleado.findAllEmpleadoes());
        }
        return "empleadoes/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public String EmpleadoController.update(@Valid Empleado empleado, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            uiModel.addAttribute("empleado", empleado);
            return "empleadoes/update";
        }
        uiModel.asMap().clear();
        empleado.merge();
        return "redirect:/empleadoes/" + encodeUrlPathSegment(empleado.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
    public String EmpleadoController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("empleado", Empleado.findEmpleado(id));
        return "empleadoes/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String EmpleadoController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Empleado.findEmpleado(id).remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/empleadoes";
    }
    
    @ModelAttribute("empleadoes")
    public Collection<Empleado> EmpleadoController.populateEmpleadoes() {
        return Empleado.findAllEmpleadoes();
    }
    
    String EmpleadoController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        }
        catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
